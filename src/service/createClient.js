console.log("Loading createClient schema service");

const dynamo = require("ebased/service/storage/dynamo");
const config = require("ebased/util/config");

const TABLE_NAME = config.get("TABLE_NAME");

const createClient = async (item) => dynamo.putItem({ TableName: TABLE_NAME, Item: item });

module.exports = { createClient };
